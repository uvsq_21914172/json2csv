# README #

## Deja fait ##



## A faire ##

* \[Fonctionel]Methode Eval permettant de calculer les attributs cible
* \[Commencé] Objet de configuration
* Main:
implémentation des arguments au lancement
* \[A tester/corriger]Fonction de conversion
* Tests:
Test d'eval
* Gestion des erreurs:
erreurs algebrique.
erreurs expression.
erreurs format.
* Edition de la Javadoc
tout
* \[Commencé] Configuration du Pom
* Utilisation de Checkstyle
* Utilisation de spotbugs


## Comment le programme devra fonctionner ##

* appel du programme

java -jar J2C.jar \[action] \[file] \[config]

Action:
	-config,
		genere un fichier de config vierge
	-csv,
		converti le fichier json \[file] en csv en suivant
		les rêgles dans \[config]
	-json,
		converti le fichier csv \[file] en json en suivant
		les rêgles dans \[config]

* format du fichier de config
exemple:
```javascript
{
csv:
	{
	${csvattr_1} : "${jsonattr_1}"
	${csvattr_2} : "${jsonattr_2} + 5"
	${csvattr_3} : "${jsonattr_2} * 3 + ( ${jsonattr_4}.${jsonattr_5} - ${jsonattr_3} )"
	}
json:
	{
	${csvattr_1} : "${csvattr_1}"
	${csvattr_2} : "${csvattr_2} + 5"
	${csvattr_3} : "${csvattr_2} * 3 + ( ${csvattr_1} - ${csvattr_3} )"
	}
}
```
* contraintes du programme

-Config:
	.Il doit toujours y avoir un espace entre chaques opérateurs/parenthese/nombres magiques/variable cibles.
	(Voir l'exemple. Ces espaces permettent de séparer les éléments de l'expression mathématique)
	.Le programme ne permet pas le retour vers un json a plusieurs niveaux.
	(A VOIR... mais flemme).
	
-Json et Csv:
	.les fichiers ne doivent contenir que des valeurs numériques.
	(A voir, on peut éventuellement modifier le fichier de config pour contenir les types des attributs cible)