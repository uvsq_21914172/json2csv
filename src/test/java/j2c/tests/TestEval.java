package j2c.tests;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import j2c.errors.J2CAlgebreException;
import j2c.errors.J2CSymboleException;
import j2c.utils.J2CMath;
/**
 * 
 * 
 *
 */
public class TestEval {
	@Test
	public void testaddition() throws J2CAlgebreException, J2CSymboleException {
		String formule = "1 + 1";
		Map<String, Object> variables = null;
		assertEquals(2, J2CMath.eval(variables, formule.split(" ")), 0.1);
	}
	
	@Test
	public void testsoustraction() throws J2CAlgebreException, J2CSymboleException{
		String formule = "1 - 1";
		Map<String, Object> variables = null;
		assertEquals(0, J2CMath.eval(variables, formule.split(" ")), 0.1);
	}
	
	@Test
	public void testmultiplication() throws J2CAlgebreException, J2CSymboleException{
		String formule = "3 * 1";
		Map<String, Object> variables = null;
		assertEquals(3, J2CMath.eval(variables, formule.split(" ")), 0.1);
	}
	
	@Test
	public void testdivision() throws J2CAlgebreException, J2CSymboleException{
		String formule = "5 / 5";
		Map<String, Object> variables = null;
		assertEquals(1, J2CMath.eval(variables, formule.split(" ")), 0.1);
	}
	
	@Test
	public void testprioritemult() throws J2CAlgebreException, J2CSymboleException{
		String formule = "1 + 0 * 2";
		Map<String, Object> variables = null;
		assertEquals(1, J2CMath.eval(variables, formule.split(" ")), 0.1);
	}
	
	@Test
	public void testprioritepar() throws J2CAlgebreException, J2CSymboleException{
		String formule = "2 * ( 1 + 1 )";
		Map<String, Object> variables = null;
		assertEquals(4, J2CMath.eval(variables, formule.split(" ")), 0.1);
	}
	
	@Test
	public void testvariable() throws J2CAlgebreException, J2CSymboleException{
		String formule = "a";
		Map<String, Object> variables = new HashMap<String, Object>();
		variables.put("a", 10.0);
		assertEquals(10, J2CMath.eval(variables, formule.split(" ")), 0.1);
	}
	
	@Test(expected = J2CAlgebreException.class)
	public void testdivisionpar0() throws J2CAlgebreException, J2CSymboleException{
		String formule = "1 / 0";
		Map<String, Object> variables = null;
		J2CMath.eval(variables, formule.split(" "));
	}
		
	@Test(expected = J2CAlgebreException.class)
	public void testmauvaisesynthaxe() throws J2CAlgebreException, J2CSymboleException{
		String formule = "1-1";
		Map<String, Object> variables = null;
		J2CMath.eval(variables, formule.split(" "));
	}
	
	@Test(expected = J2CSymboleException.class)
	public void testvariablenonexistante() throws J2CAlgebreException, J2CSymboleException{
		String formule = "a - 1";
		Map<String, Object> variables = new HashMap<String, Object>();
		J2CMath.eval(variables, formule.split(" "));
	}

}
