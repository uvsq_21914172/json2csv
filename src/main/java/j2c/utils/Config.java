package j2c.utils;

import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Iterator;

import org.json.JSONException;
import org.json.JSONObject;

public class Config {
	/***
	 * Config chargée a partir du fichier
	 */
	private JSONObject config;

	/***
	 * Constructeur
	 * @param jsonconfig
	 */
	public Config(JSONObject jsonconfig) {
		this.config = jsonconfig;
	}
	
	/***
	 * Renvoi le Json corespondant a la configuration
	 * @return config
	 */
	public JSONObject getConfig() {
		return config;
	}
	
	/***
	 * Charge une configuration a partir du nom d'un fichier
	 * @param FileName nom du fichier chargé
	 * @return objet config
	 * @throws IOException Exception lancée en cas de probleme avec l'ecriture ou la lecture de fichier.
	 */
	public static Config loadConfigFile(String FileName) throws IOException {
		return new Config(J2CTools.loadJSONFile(FileName));
	}
	
	/***
	 * Vérifie la validitée du fichier de configuration
	 * @param jsonconfig objet jsonObject
	 * @return boolean
	 */
	public static boolean verifyConfig(JSONObject jsonconfig) {
		Iterator<String> a;
		String attrc;
		if (jsonconfig.has("csv")){
			a = jsonconfig.getJSONObject("csv").keys();
			while(a.hasNext()) {
				attrc = a.next();
				if(!J2CMath.verify(jsonconfig.getJSONObject("csv").getString(attrc).split(" "))) {
					System.err.println("Mauvaise expression a csv."+attrc);
					return false;
				}
			}
		}
		if (jsonconfig.has("json")){
			a = jsonconfig.getJSONObject("json").keys();
			while(a.hasNext()) {
				attrc = a.next();
				if(!J2CMath.verify(jsonconfig.getJSONObject("json").getString(attrc).split(" "))) {
					System.err.println("Mauvaise expression a json."+attrc);
					return false;
				}
			}
		}
		return true;
	}
	
	/***
	 * Genere un fichier json d'exemple au format de la config
	 * @throws JSONException
	 * @throws IOException
	 */
	public static void defaultConfigFile() throws JSONException, IOException
	{
		JSONObject attrex = new JSONObject();
		attrex.put("attrex", "2 * (1 + 1)");
		JSONObject json = new JSONObject();
		json.put("csv",attrex);
		json.put("json",attrex);
		Writer writer = Files.newBufferedWriter(Paths.get("default_config.json"));
		writer.write(json.toString(4));
		writer.close();
	}
}
