package j2c.utils;

import j2c.errors.J2CAlgebreException;

public enum Operateur {
	
	PLUS("+"){
		@Override
		double eval(double op1, double op2) {
			return op1 + op2;
		}
	},
	MOINS("-"){
		@Override
		double eval(double op1, double op2) {
			return op2 - op1;
		}
	},
	MULT("*"){
		@Override
		double eval(double op1, double op2) {
			return op1 * op2;
		}
	},
	DIV("/"){
		@Override
		double eval(double op1, double op2) throws J2CAlgebreException {
			if(op1 == 0)
				throw new J2CAlgebreException("Tentative de division par zero!");
			return op2 / op1;
		}
	},
	NONE{
		@Override
		double eval(double op1, double op2){
			return 0;
		}
	};

	private String symbole;
	
	/**
	 * Constructeur de l'operation
	 * @params symbole
	 */
	
	private Operateur(String symbole) {
		this.symbole = symbole;
	}
	
	/**
	 * Constructeur de l'operation
	 * @params symbole
	 */
	
	private Operateur() {
	}
	
	/**
	 * Operation correspondant a l'enum
	 * @params Operande en haut de pile
	 * @params Second operande
	 * @return le resultat de l'operation
	 * @throws J2CAlgebreException 
	 */
	
	abstract double eval(double op1, double op2) throws J2CAlgebreException;
	/**
	 * Fonction renvoyant l'enum correspondant au symbole
	 * @param symbole valeur representant l'operateur
	 * @return l'operation correspondante ou NONE
	 */
	public static Operateur opFromSymbole(String symbole) {
		if(symbole.equals(PLUS.symbole))
			return PLUS;
		else if(symbole.equals(MOINS.symbole))
			return MOINS;
		else if(symbole.equals(MULT.symbole))
			return MULT;
		else if(symbole.equals(DIV.symbole))
			return DIV;
		else
			return NONE;
	}
}
