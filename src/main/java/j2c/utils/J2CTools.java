package j2c.utils;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import com.opencsv.exceptions.CsvValidationException;
import j2c.errors.J2CException;
import j2c.errors.J2CSymboleException;

public class J2CTools {
	
	/***
	 * Fonction permettant d'extraire des double dans un ensemble de maps imbriquées
	 * @param map Objet map contenant les valeurs des variables
	 * @param key Clé complete de l'objet cherché
	 * @return Double recherché
	 * @throws J2CSymboleException Si le symbole n'est pas présent dans la map
	 */
	@SuppressWarnings("unchecked")
	public static double getDouble(Map<String, Object> map, String key) throws J2CSymboleException{
		String[] keys = key.split("[.]");
		Object tmp;
		for(int i = 0; i < keys.length-1; i++) {
			if(map.containsKey(keys[i]) && (tmp = map.get(keys[i])) instanceof Map<?, ?>)
				map = (Map<String, Object>)(tmp);
			else
				throw new J2CSymboleException("Le symbole " + key + " n'existe pas dans l'objet traité");
		}
		if(map.containsKey(keys[keys.length-1])) {
			if((tmp = map.get(keys[keys.length-1])) instanceof Number)
				return ((Number)tmp).doubleValue();
		}else {
			throw new J2CSymboleException("Le symbole " + key + " n'existe pas dans l'objet traité");
		}
		throw new J2CSymboleException("Le symbole \"" + key + "\" n'est pas un nombre.");
	}
	
	/***
	 * Fonction permettant de charger un fichier Json
	 * @param FileName Chemin du fichier
	 * @return JsonObject chargé
	 * @throws IOException Exception lancée en cas de probleme avec l'ecriture ou la lecture de fichier.
	 */
	public static JSONObject loadJSONFile(String FileName) throws IOException {
        File f = new File(FileName);
        if (f.exists()){
            byte[] encoded = Files.readAllBytes(Paths.get(FileName));
            String jsonTxt = new String(encoded, Charset.forName("UTF-8"));
            JSONObject json = new JSONObject(jsonTxt);  
            return json;
        }
        System.err.println("Le fichier "+FileName+"n'existe pas.");
        return null;
	}
	
	/***
	 * Fonction permettant de charger un fichier Json
	 * @param FileName Chemin du fichier
	 * @return JsonArray chargé
	 * @throws IOException Exception lancée en cas de probleme avec l'ecriture ou la lecture de fichier.
	 */
	public static JSONArray loadJSONArrayFile(String FileName) throws IOException {
        File f = new File(FileName);
        if (f.exists()){
            byte[] encoded = Files.readAllBytes(Paths.get(FileName));
            String jsonTxt = new String(encoded, Charset.forName("UTF-8"));
            JSONArray json = new JSONArray(jsonTxt);  
            return json;
        }
        System.err.println("Le fichier "+FileName+"n'existe pas.");
        return null;
	}
	
	/***
	 * Fonction parcourant le fichier json et le convertissant en csv
	 * @param json json a convertir
	 * @param conf configuration
	 * @param FileName non du fichier csv généré
	 * @throws CsvValidationException ..
	 * @throws IOException Exception lancée en cas de probleme avec l'ecriture ou la lecture de fichier.
	 * @throws JSONException ..
	 */
	public static void jsonToCsv(JSONArray json, Config conf, String FileName) throws CsvValidationException, IOException, JSONException{
		Writer writer = Files.newBufferedWriter(Paths.get(FileName));
		CSVWriter csv = new CSVWriter(writer);
		JSONObject rules = conf.getConfig().getJSONObject("csv");
		Double d;
		int rejets = 0;
		int msg = 0;
		Iterator<String> a;
		ArrayList<String> tmp;
		String[] nextline;
		String attrc;
		nextline = (String[]) rules.keySet().toArray(new String[rules.length()]);
		csv.writeNext(nextline);
		for(int i = 0; i < json.length(); i++) {
			a = rules.keys();
			tmp = new ArrayList<String>();
			msg++;
			try {
				while(a.hasNext()) {
					attrc = a.next();
					d = J2CMath.eval(json.getJSONObject(i), rules.getString(attrc));
					tmp.add(d.toString());
				}
				nextline = (String[]) tmp.toArray(new String[rules.length()]);
				csv.writeNext(nextline);
			}catch(J2CException e) {
				rejets++;
				System.err.println("Conversion de "+rejets+" message annulé(msg n°"+ msg +")");
				System.err.println(e.getMessage());
			}
		}
		csv.close();
	}
	
	/***
	 * Fonction permettant de charger un fichier Csv
	 * @param FileName Chemin du fichier
	 * @return CSVReader généré
	 * @throws IOException Exception lancée en cas de probleme avec l'ecriture ou la lecture de fichier.
	 */
	public static CSVReader loadCSVFile(String FileName) throws IOException {
		Reader reader = Files.newBufferedReader(Paths.get(FileName));
		CSVReader csvReader = new CSVReader(reader);
		return csvReader;
	}
	
	/***
	 * Fonction parcourant le fichier csv et le convertissant en json
	 * @param csv csv a convertir
	 * @param conf config de la conversion
	 * @param FileName nom du fichier a génerer
	 * @throws CsvValidationException Erreur pouvant arriver si le json entré est incorrect
	 * @throws IOException Erreur au niveau de l'ecriture ou la lecture d'un fichier
	 * @throws JSONException Erreur au niveau du json généré
	 */
	public static void csvToJSON(CSVReader csv, Config conf, String FileName) throws CsvValidationException, IOException, JSONException{
		JSONArray ja = new JSONArray();
		JSONObject rules = conf.getConfig().getJSONObject("json");
		JSONObject tmp;
		int rejets = 0;
		int msg = 0;
		Iterator<String> a;
		Map<String, Object> tuple;
		String[] header = csv.readNext();
		String[] nextline;
		String attrc;
		while((nextline = csv.readNext()) != null) {
			tmp = new JSONObject();
			tuple = new HashMap<String, Object>();
			msg++;
			try {
			for(int i = 0; i < header.length; i++) {
				tuple.put(header[i], Double.parseDouble(nextline[i]));
			}
			a = rules.keys();
			while(a.hasNext()) {
				attrc = a.next();
				tmp.put(attrc, J2CMath.eval(tuple, rules.getString(attrc).split(" ")));
			}
			ja.put(tmp);
			}catch(J2CException e) {
				rejets++;
				System.err.println("Conversion de "+rejets+" message annulé(msg n°"+ msg +")");
				System.err.println(e.getMessage());
			}
		}
		Writer writer = Files.newBufferedWriter(Paths.get(FileName));
		writer.write(ja.toString(4));
		writer.close();
	}
	
	
	
}

