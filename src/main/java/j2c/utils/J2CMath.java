package j2c.utils;

import java.util.Arrays;
import java.util.Map;
import java.util.Stack;

import org.json.JSONObject;

import j2c.errors.J2CAlgebreException;
import j2c.errors.J2CSymboleException;

public class J2CMath {
	
	/***
	 * Fonction permettant de calculer le resultat d'une expression mathématique
	 * @param json json contenant les variables a calculer
	 * @param formule formule mathématique a évaluer
	 * @return retourne le résultat du calcul
	 * @throws J2CAlgebreException ..
	 * @throws J2CSymboleException ..
	 */
	public static double eval(JSONObject json, String formule) throws J2CAlgebreException, J2CSymboleException{
		String[] far = formule.split(" ");
		return eval(json.toMap(), far);
	}
	
	/***
	 * Fonction permettant de calculer le resultat d'une expression mathématique
	 * @param values map contenant les variables a calculer
	 * @param far expression dont les éléments sont séparé dans un tableau
	 * @return retourne le résultat
	 * @throws J2CAlgebreException ..
	 * @throws J2CSymboleException ..
	 */
	public static double eval(Map<String, Object> values, String[] far) throws J2CAlgebreException, J2CSymboleException{
		Stack<Double> operandes = new Stack<Double>();
		Stack<Operateur> operateurs = new Stack<Operateur>();
		Operateur op;
		for (int i = 0; i< far.length; i++) {
			if(!(op = Operateur.opFromSymbole(far[i])).equals(Operateur.NONE)) {
				operateurs.push(op);
			}
			else{
				if(far[i].equals("(")) {
					int a = 1;
					int j = i + 1;
					while(a > 0)
					{
					    if(j >= far.length)
					    	throw new J2CAlgebreException("Erreur de syntaxe(Parenthese)");
						else if (far[j].equals("("))
							a++;
						else if (far[j].equals(")"))
							a--;
						j++;
					}
					operandes.push(eval(values, Arrays.copyOfRange(far, i+1, j-1)));
					i = j-1;
				}
				else if(far[i].matches("^[a-zA-Z](.)*$")) {
					operandes.push(J2CTools.getDouble(values, far[i]));
				}
				else if(far[i].matches("^(\\+|\\-|)?(([1-9][0-9]*)|[0])([.][0-9]+)?$")){
					operandes.push(Double.parseDouble(far[i]));
				}
				else {
					throw new J2CAlgebreException("Erreur de syntaxe(Symbole inconnu)");
				}
				if(!operateurs.empty() && (operateurs.peek().equals(Operateur.MULT) ||
						operateurs.peek().equals(Operateur.DIV))) {
					operandes.push(operateurs.pop().eval(operandes.pop(), operandes.pop()));
				}
			}
		}
		while(!operateurs.empty())
			operandes.push(operateurs.pop().eval(operandes.pop(), operandes.pop()));
		return operandes.peek();
	}
	
	/***
	 * Verifie la validité d'une expression mathématique
	 * @param far expression dont les éléments sont séparé dans un tableau
	 * @return retourne vrai si l'expression est correct
	 */
	public static boolean verify(String[] far) {
		boolean o = true;
		for (int i = 0; i< far.length; i++) {
			if(!Operateur.opFromSymbole(far[i]).equals(Operateur.NONE)) {
				o = true;
			}
			else if(o == true){
				o = false;
				if(far[i].equals("(")) {
					int a = 1;
					int j = i + 1;
					while(a > 0)
					{
					    if(j >= far.length)
					    	return false;
						else if (far[j].equals("("))
							a++;
						else if (far[j].equals(")"))
							a--;
						j++;
					}
					if(!verify(Arrays.copyOfRange(far, i+1, j-1)))
						return false;
					i = j-1;
				}
				else if(!far[i].matches("^[a-zA-Z](.)*$") && 
						!far[i].matches("^(\\+|\\-|)?(([1-9][0-9]*)|[0])([.][0-9]+)?$")){
					return false;
				}
			}else {
				return false;
			}
		}
		return true;
	}
}
