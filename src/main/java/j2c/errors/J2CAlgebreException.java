package j2c.errors;

@SuppressWarnings("serial")
public class J2CAlgebreException extends J2CException {

	/***
	 * Constructeur de l'exception appellé en cas d'erreurs algébriques.
	 * @param message Message envoyé au moment du throw.
	 */
	public J2CAlgebreException(String message) {
		super(message);
	}

}
