package j2c.errors;

@SuppressWarnings("serial")
public class J2CException extends Exception {

	/***
	 * Constructeur de l'exception mere des exceptions propre a ce programme.
	 * @param message Message envoyé au moment du throw.
	 */
	public J2CException(String message) {
		super(message);
	}

}
