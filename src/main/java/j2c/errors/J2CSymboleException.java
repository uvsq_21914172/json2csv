package j2c.errors;

@SuppressWarnings("serial")
public class J2CSymboleException extends J2CException {

	/***
	 * Constructeur de l'exception appellé en cas d'erreurs de variabes.
	 * @param message Message envoyé au moment du throw.
	 */
	public J2CSymboleException(String message) {
		super(message);
	}

}
