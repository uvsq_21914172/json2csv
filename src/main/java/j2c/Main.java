package j2c;

import java.io.IOException;

import org.json.JSONException;
import com.opencsv.exceptions.CsvValidationException;

import j2c.utils.Config;
import j2c.utils.J2CTools;

public class Main {
	
	/***
	 * Pour eviter une des erreurs de checkstyle
	 */
	private Main() {
		//Pour eviter une des erreurs de checkstyle
	}
	
	
	/***
	 * Fonction Main.
	 * @param args argument du main.
	 * @throws JSONException Exception lancée en cas de probleme avec le chargement du fichier Json.
	 * @throws IOException Exception lancée en cas de probleme avec l'ecriture ou la lecture de fichier.
	 * @throws CsvValidationException Exception lancée en cas de probleme avec le chargement du fichier CSV.
	 */
	public static void main(String[] args) throws  JSONException, IOException, CsvValidationException {
		String name = "output";
		Config conf;
		if(args.length >= 1) {
			if(args[0].equals("config")) {
				System.out.println("Génération du fichier de config");
				Config.defaultConfigFile();
			}
			else if(args[0].equals("csv")) {
				if (args.length >= 3) {
					conf = Config.loadConfigFile(args[2]);
					if(args.length >= 4) {
						name = args[3];
					}else {
						name = name + ".txt";
					}
					if(Config.verifyConfig(conf.getConfig()) &&
							conf.getConfig().has("csv") &&
							conf.getConfig().getJSONObject("csv").length() > 0){
						J2CTools.jsonToCsv(J2CTools.loadJSONArrayFile(args[1]), Config.loadConfigFile(args[2]), name);
					}else
						System.out.println("Le fichier de config n'est pas correct");
				}
				else
					System.out.println("Nombre de parametres Incorects");
			}else if(args[0].equals("json")) {
				if (args.length >= 3) {
					conf = Config.loadConfigFile(args[2]);
					if(args.length >= 4) {
						name = args[3];
					}else {
						name = name + ".json";
					}
					if(Config.verifyConfig(conf.getConfig()) 
							&& conf.getConfig().has("json") 
							&& conf.getConfig().getJSONObject("json").length() > 0){
						J2CTools.csvToJSON(J2CTools.loadCSVFile(args[1]), conf, name);
					}else
						System.out.println("Le fichier de config n'est pas correct");
					
				}
				else
					System.out.println("Nombre de parametres Incorects");
			}else
				System.out.println("Mauvais mot cle au premier attribut");
		}
		else
			System.out.println("Nombre de parametres Incorects");
	}
}
